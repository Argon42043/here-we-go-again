# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.9] - 2025-02-16

### Changed
- change big cannon spring wire recipe (Fix issue [#109](https://gitlab.com/gaslighting-labs/minecraft/modpacks/here-we-go-again/-/issues/109))
- adding steel recipe for big cannon (Fix issue [#108](https://gitlab.com/gaslighting-labs/minecraft/modpacks/here-we-go-again/-/issues/108))

## [2.0.8] - 2025-02-13

### Changed
- Updated Create Numismatics
- Updated Create 

## [2.0.7] - 2024-11-22

### Changed
- Updated Perfect Plushies Fix

## [2.0.6] - 2024-10-10

### Added
- Perfect Plushies Fix

### Changed
- updated Perfext Plushies
- updated CopyCats
- Fix train time

## [2.0.5] - 2024-09-01

### Added
- Daily-Rewards
- Missions

### Changed
- replaced Ryoamiclights with Dynamiclights-Reforged
- changed combat configuration of Minecolonies Server-Config

## [2.0.4] - 2024-08-26

### Added
- money distribution

### Fixed
- lightning and phantoms staying in the sky

## [2.0.3] - 2024-08-24

### Added
- Login protection

### Changed
- Hostility relation to other players, mobs and entities

## [2.0.2] - 2024-08-17

### Added
- Connectored Concurrent Chunk Management Engine
- Custom Item Textures
- Custom resource pack
- Custom emojis

### Changed
- replaced Canary with Radium

### Removed
- Dynamic View
- Better chunk loading

## [2.0.1] - 2024-08-13

### Added
- added account switcher
- made colliding crafting recipes selectable
- improved description for optional mods

### Changed
- changed packwiz deployment
- Rearranged small buttons in the main menu
- new menu backgrounds
- replaced farsight with bobby
- replaced dynamic light with ryonamiclights
- reduced minimum memory warning
- replaced SOL-Apple-Pie with SOL-Onion
- reduced AFK detection time
- updated minecolonies
- made some more mods optional

### Removed
- hide unobtainable weapons

### Fixed
- disabled bclib's autosync
- Fixed eye animations
- fixed not ticking enderpearls
- fixed network cable recipe

## [2.0.0] - 2024-07-26

### Features

#### Uncategorized Features
- 1.20.1 features
- Advanced contraption controller with game controller support
- Ambient Sounds
- Analog Camera with film rolls, filters, a Lightroom for photo development, and much more
- Cars have been replaced by new cars, tractors and some other kinds of vehicles, including accessories
- Cauldrons can be used to mix potions and dyes
- Different baby types
- Elytras are more customizable with trims, dyes and banners
- Fishing minigame
- More archeology features including new structures, fossils, ancient enchantments and much more
- More weathering stuff
- Name stars and constellations (optional)
- New create styled money mod
- Now the wither is stronger and has changed its strategy
- Playable music instruments
- Plushies
- Sponge absorbs more water
- Text color and font can be changed when writing a book on a lectern
- You can still move around while in your inventory

#### Chat
- Chat menu got some animations
- Emojis in messages
- Messages show the sender's head
- Toggle team and global chat with the press of a button

#### World
- Increased Nether height to 256 blocks
- New structures and biomes in the end dimension
- New underground caves and underwater trenches
- Overworld generation changed
- Snow generates under trees
- Structures and biomes changed in the nether dimension
- The nether-to-overworld distance is now 1:1

#### Food
- Corn crop and some food made from it
- Nether relate food and ingredients
- Real cooking in the pan (turn the food in the pan, otherwise it will burn)
- Some more sweets

#### Visuals
- Blur effect in menus
- Camera now follows the train when riding it
- Clouds are less blocky and more diverse
- Different explosion animations (optional)
- Glowing effect similar to shaders (optional)
- Item swapping animation (optional)
- Main menu redesign
- Menu sliding animations in inventory, chests, etc. (optional)
- New loading screen
- Player camera leaning (optional)
- Some more environmental particles
- Splash title upon dimension switch (optional)
- Unique enchantment book textures
- Wakes in water (optional)

#### Decoration / Building
- Banners can be hung from the ceiling
- Cake can be placed on top of another cake
- Candles can be placed on mob heads
- Carpets can be placed on slabs and stairs
- Chains can be spanned between fence posts
- Flowerpots can be hung from the ceiling
- Items can be placed on hanging signs
- Mob heads can be placed on top of each other
- More copycats than you ever need
- Some tools can be placed on tripwire hooks

#### Management
- AFK detection and notification
- Permission system
- Ping locations in the world

### Removed

- Mod: create ruins
- Mod: create sandpaper overhaul
- Mod: creatures and beasts
- Mod: eating animations
- Mod: hexcasting
- Mod: incendium
- Mod: modern train parts
- Mod: more babies
- Mod: numismatic overhaul
- Mod: tectonic
- Mod: ultimate car mod
- Mod: vertical slabs compat
