<div align="center">

# Here we go again

![GitLab (self-managed) contributors](https://img.shields.io/gitlab/contributors/gaslighting-labs%2Fminecraft%2Fmodpacks%2Fhere-we-go-again)
![GitLab issues by-label](https://img.shields.io/gitlab/issues/open/gaslighting-labs%2Fminecraft%2Fmodpacks%2Fhere-we-go-again)
![GitLab merge requests by-label](https://img.shields.io/gitlab/merge-requests/open/gaslighting-labs%2Fminecraft%2Fmodpacks%2Fhere-we-go-again)
![GitLab stars](https://img.shields.io/gitlab/stars/gaslighting-labs%2Fminecraft%2Fmodpacks%2Fhere-we-go-again)

This Minecraft modpack is designed to create an even more immersive and enhanced gaming experience by improving existing game mechanics and implementing new ones, such as colony building, real factories, and road and train infrastructure.

[![Getting Started](https://dabuttonfactory.com/button.png?t=Getting+Started&f=Open+Sans-Bold&ts=26&tc=fff&hp=45&vp=20&c=11&bgt=gradient&bgc=00e6ff&ebgc=3a6b97&be=1)](https://gaslighting-labs.gitlab.io/minecraft/modpacks/here-we-go-again/anmeldung.html)

</div>

## Development

### Setup

#### Enable git hooks
```sh
git config core.hooksPath .githooks/
```

#### Start development server
```sh
cd packwiz
packwiz serve
```

### Related Software

- [Minecraft Java](https://www.minecraft.net/)
- [Minecraft Forge](https://files.minecraftforge.net/net/minecraftforge/forge/)
- [Minecraft Server Docker](https://docker-minecraft-server.readthedocs.io/)
- [packwiz](https://packwiz.infra.link/)


### Related Versions

![Static Badge](https://img.shields.io/badge/Minecraft_Version-1.20.1-green)\
![Static Badge](https://img.shields.io/badge/Forge_Version-47.3.0-red)
