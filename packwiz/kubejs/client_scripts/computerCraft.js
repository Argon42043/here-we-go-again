JEIEvents.hideItems(event => {
    event.hide('advancedperipherals:me_bridge')
    event.hide('advancedperipherals:rs_bridge')
    event.hide('computercraft:computer_command')
})
