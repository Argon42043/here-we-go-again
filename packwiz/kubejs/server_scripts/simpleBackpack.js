ServerEvents.recipes(event => {
  event.remove({ output: 'backpacked:backpack'})
})

ServerEvents.recipes(event => {
  event.shaped(
    Item.of('backpacked:backpack', 1),
    [ 
      'SeS', 
      'sIs', 
      'PeP'  
    ],
    {
      I: 'minecraft:iron_ingot', 
      S: 'minecraft:string',  
      s: 'sewingkit:leather_strip',
      e: 'sewingkit:leather_sheet',
      P: 'toolbelt:pouch'
    }
  )
})
