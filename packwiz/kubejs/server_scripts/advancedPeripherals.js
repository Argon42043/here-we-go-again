ServerEvents.recipes(event => {
    //unused item
    event.remove({ output: 'advancedperipherals:computer_tool' })
    //to powerfull items
    event.remove({ output: 'advancedperipherals:weak_automata_core' })
    event.remove({ output: 'advancedperipherals:overpowered_weak_automata_core' })
    event.remove({ output: 'advancedperipherals:overpowered_husbandry_automata_core' })
    event.remove({ output: 'advancedperipherals:overpowered_end_automata_core' })
})

ServerEvents.recipes(event => {
    event.recipes.create.mechanical_crafting('advancedperipherals:weak_automata_core', [
        'BBsBB',
        'BRCRB',
        'pCUCa',
        'BRCRB',
        'BHSHB'
    ], {
        U: 'endrem:undead_soul',
        C: 'advancedperipherals:peripheral_casing',
        R: 'minecraft:redstone_block',
        S: 'minecraft:netherite_upgrade_smithing_template',
        B: 'create:brass_block',
        H: 'quark:diamond_heart',
        s: 'minecraft:diamond_sword',
        a: 'minecraft:diamond_axe',
        p: 'minecraft:diamond_pickaxe'
    })

    event.recipes.create.mechanical_crafting('advancedperipherals:overpowered_weak_automata_core', [
        'BSB',
        'NWN',
        'BSB',
    ], {
        W: 'advancedperipherals:weak_automata_core',
        N: 'minecraft:nether_star',
        S: 'minecraft:netherite_upgrade_smithing_template',
        B: 'create:brass_block'
    })

    event.recipes.create.mechanical_crafting('advancedperipherals:overpowered_husbandry_automata_core', [
        'BSB',
        'NWN',
        'BSB',
    ], {
        W: 'advancedperipherals:husbandry_automata_core',
        N: 'minecraft:nether_star',
        S: 'minecraft:netherite_upgrade_smithing_template',
        B: 'create:brass_block'
    })

    event.recipes.create.mechanical_crafting('advancedperipherals:overpowered_end_automata_core', [
        'BSB',
        'NWN',
        'BSB',
    ], {
        W: 'advancedperipherals:end_automata_core',
        N: 'minecraft:nether_star',
        S: 'minecraft:netherite_upgrade_smithing_template',
        B: 'create:brass_block'
    })
})