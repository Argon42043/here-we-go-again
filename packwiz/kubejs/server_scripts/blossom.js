ServerEvents.recipes(e => {
    e.recipes.create.mixing([Item.of('quark:yellow_blossom_sapling').withChance(0.1), 'dirt'], ['dirt', Fluid.water(1000), '#forge:dyes/yellow', 'immersive_weathering:azalea_flowers'])
    e.recipes.create.mixing([Item.of('quark:orange_blossom_sapling').withChance(0.1), 'dirt'], ['dirt', Fluid.water(1000), '#forge:dyes/orange', 'immersive_weathering:azalea_flowers'])
    e.recipes.create.mixing([Item.of('quark:red_blossom_sapling').withChance(0.1), 'dirt'], ['dirt', Fluid.water(1000), '#forge:dyes/red', 'immersive_weathering:azalea_flowers'])
    e.recipes.create.mixing([Item.of('quark:pink_blossom_sapling').withChance(0.1), 'dirt'], ['dirt', Fluid.water(1000), '#forge:dyes/pink', 'immersive_weathering:azalea_flowers'])
    e.recipes.create.mixing([Item.of('quark:lavender_blossom_sapling').withChance(0.1), 'dirt'], ['dirt', Fluid.water(1000), '#forge:dyes/purple', 'immersive_weathering:azalea_flowers'])
    e.recipes.create.mixing([Item.of('quark:blue_blossom_sapling').withChance(0.1), 'dirt'], ['dirt', Fluid.water(1000), '#forge:dyes/light_blue', 'immersive_weathering:azalea_flowers'])
})
