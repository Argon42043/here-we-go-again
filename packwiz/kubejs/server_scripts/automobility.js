ServerEvents.recipes(event => {
    event.remove({ output: 'automobility:auto_mechanic_table' })
    event.remove({ output: 'automobility:automobile_assembler' })
})

ServerEvents.recipes(event => {
    event.recipes.create.mechanical_crafting('automobility:auto_mechanic_table', [
        'CCCCC',
        'CPwPC',
        'OMWMO',
        'OwMwO',
        'OMAMO'
    ], {
        C: 'minecraft:copper_ingot',
        P: 'create:mechanical_piston',
        w: 'create:cogwheel',
        W: 'create:large_cogwheel',
        M: 'create:mechanical_crafter',
        A: 'automobility:crowbar',
        O: 'minecraft:oak_wood'
    })

    event.recipes.create.mechanical_crafting('automobility:automobile_assembler', [
        'CCCCCCC',
        'CiiiiiC',
        '  IiI  ',
        ' IIiII ',
        'IIiiiII'
    ], {
        C: 'minecraft:copper_block',
        I: 'create:industrial_iron_block',
        i: 'minecraft:iron_ingot'
    })
})
