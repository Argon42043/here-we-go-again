// Listen to item tag event
ServerEvents.tags('item', event => {


    //Define all Plusheis
    const itemsToModify = [
        'perfectplushies:red_fox_plushie',
        'perfectplushies:snow_fox_plushie',
        'perfectplushies:raccoon_plushie',
        'perfectplushies:red_panda_plushie',
        'perfectplushies:red_ruffed_lemur_plushie',
        'perfectplushies:capybara_plushie',
        'perfectplushies:dog_plushie',
        'perfectplushies:cat_plushie',
        'perfectplushies:dolphin_plushie',
        'perfectplushies:rabbit_plushie',
        'perfectplushies:frog_plushie',
        'perfectplushies:goose_plushie',
        'perfectplushies:duck_plushie',
        'perfectplushies:rubber_duck_plushie',
        'perfectplushies:robin_plushie',
        'perfectplushies:hummingbird_plushie',
        'perfectplushies:hippo_plushie',
        'perfectplushies:mouse_plushie',
        'perfectplushies:turtle_plushie',
        'perfectplushies:doe_plushie',
        'perfectplushies:reindeer_plushie',
        'perfectplushies:bear_plushie',
        'perfectplushies:panda_plushie',
        'perfectplushies:lion_cub_plushie',
        'perfectplushies:elephant_plushie',
        'perfectplushies:monkey_plushie',
        'perfectplushies:seal_plushie',
        'perfectplushies:nyf_plushie',
        'perfectplushies:sirjain_plushie',
        'perfectplushies:sizableshrimp_plushie',
        'perfectplushies:tslat_plushie',
        'perfectplushies:silk_plushie',
        'perfectplushies:june_plushie',
        'perfectplushies:daniel_plushie',
        'perfectplushies:gamerpotion_plushie',
        'perfectplushies:joosh_plushie',
        'perfectplushies:rocris_plushie',
        'perfectplushies:geode_plushie',
        'perfectplushies:gamerpotion_plushie_rare',
        'perfectplushies:sirjain_plushie_rare',
      ];
    
      //Loop through the list and remove all tags from each item
      itemsToModify.forEach(item => {
        event.removeAllTagsFrom(item);
      })
  })