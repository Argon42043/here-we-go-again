ServerEvents.recipes(event => {
    event.remove({ output: 'handcrafted:white_sheet'})
})
  
ServerEvents.recipes(event => {
    event.shaped(
        Item.of('handcrafted:white_sheet', 2),
        [
            '  W',
            ' W ',
            'W  '
        ],
        {
            W: 'minecraft:white_wool'
        }
    )
})
