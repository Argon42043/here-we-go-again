var completlyRemove = [
    'simple_weapons:fiery_scythe',
    'simple_weapons:fiery_dagger',
    'simple_weapons:fiery_greatsword',
    'simple_weapons:fiery_spear',
    'simple_weapons:fiery_fist',
    'simple_weapons:fiery_scimitar',
    'simple_weapons:fiery_sickle',
    'simple_weapons:fiery_katana',
    'simple_weapons:knightmetal_scythe',
    'simple_weapons:knightmetal_dagger',
    'simple_weapons:knightmetal_greatsword',
    'simple_weapons:knightmetal_spear',
    'simple_weapons:knightmetal_fist',
    'simple_weapons:knightmetal_scimitar',
    'simple_weapons:knightmetal_sickle',
    'simple_weapons:knightmetal_katana',
    'simple_weapons:steeleaf_scythe',
    'simple_weapons:steeleaf_dagger',
    'simple_weapons:steeleaf_greatsword',
    'simple_weapons:steeleaf_spear',
    'simple_weapons:steeleaf_fist',
    'simple_weapons:steeleaf_scimitar',
    'simple_weapons:steeleaf_sickle',
    'simple_weapons:steeleaf_katana',
    'simple_weapons:ironwood_scythe',
    'simple_weapons:ironwood_dagger',
    'simple_weapons:ironwood_greatsword',
    'simple_weapons:ironwood_spear',
    'simple_weapons:ironwood_fist',
    'simple_weapons:ironwood_scimitar',
    'simple_weapons:ironwood_sickle',
    'simple_weapons:ironwood_katana',
    'simple_weapons:pendorite_scythe',
    'simple_weapons:pendorite_dagger',
    'simple_weapons:pendorite_greatsword',
    'simple_weapons:pendorite_spear',
    'simple_weapons:pendorite_fist',
    'simple_weapons:pendorite_scimitar',
    'simple_weapons:pendorite_sickle',
    'simple_weapons:pendorite_katana',
    'simple_weapons:adamantite_scythe',
    'simple_weapons:adamantite_dagger',
    'simple_weapons:adamantite_greatsword',
    'simple_weapons:adamantite_spear',
    'simple_weapons:adamantite_fist',
    'simple_weapons:adamantite_scimitar',
    'simple_weapons:adamantite_sickle',
    'simple_weapons:adamantite_katana',
    'simple_weapons:corinthium_scythe',
    'simple_weapons:corinthium_dagger',
    'simple_weapons:corinthium_greatsword',
    'simple_weapons:corinthium_spear',
    'simple_weapons:corinthium_fist',
    'simple_weapons:corinthium_scimitar',
    'simple_weapons:corinthium_sickle',
    'simple_weapons:corinthium_katana',
    'simple_weapons:diamond_sickle',
    'simple_weapons:iron_sickle',
    'simple_weapons:netherite_sickle',
    'simple_weapons:electrum_sickle',
    'simple_weapons:warden_sickle',
    'simple_weapons:stone_sickle',
]
ServerEvents.recipes(event => {
    completlyRemove.forEach((completlyRemove) => {
        event.remove({ output: completlyRemove })
    })
})
var ItemRemove = [
    'simple_weapons:warden_scythe',
    'simple_weapons:warden_dagger',
    'simple_weapons:warden_greatsword',
    'simple_weapons:warden_spear',
    'simple_weapons:warden_fist',
    'simple_weapons:warden_scimitar',
    'simple_weapons:warden_sickle',
    'simple_weapons:warden_katana',
    'simple_weapons:electrum_scythe',
    'simple_weapons:electrum_dagger',
    'simple_weapons:electrum_greatsword',
    'simple_weapons:electrum_spear',
    'simple_weapons:electrum_fist',
    'simple_weapons:electrum_scimitar',
    'simple_weapons:electrum_sickle',
    'simple_weapons:electrum_katana',
]
ServerEvents.recipes(event => {
    ItemRemove.forEach((ItemRemove) => {
        event.remove({ output: ItemRemove })
    })
})
let createItems = (itemName, ingredient) => {
    ServerEvents.recipes(event => {
        if (itemName.includes('_scythe')) {
            event.recipes.create.mechanical_crafting(itemName, [
                'III',
                ' SI',
                ' S ',
            ], {
                S: 'minecraft:stick',
                I: ingredient
            })
        }
        if (itemName.includes('_dagger')) {
            event.recipes.create.mechanical_crafting(itemName, [
                ' I ',
                'S  ',
                '   ',
            ], {
                S: 'minecraft:stick',
                I: ingredient
            })
        }

        if (itemName.includes('_greatsword')) {
            event.recipes.create.mechanical_crafting(itemName, [
                ' I ',
                ' I ',
                'ISI',
            ], {
                S: 'minecraft:stick',
                I: ingredient
            })
        }

        if (itemName.includes('_spear')) {
            event.recipes.create.mechanical_crafting(itemName, [
                ' II',
                ' SI',
                'S  ',
            ], {
                S: 'minecraft:stick',
                I: ingredient
            })
        }

        if (itemName.includes('_fist')) {
            event.recipes.create.mechanical_crafting(itemName, [
                'SI ',
                ' S ',
                '   ',
            ], {
                S: 'minecraft:stick',
                I: ingredient
            })
        }

        if (itemName.includes('_scimitar')) {
            event.recipes.create.mechanical_crafting(itemName, [
                ' I ',
                'I  ',
                'S  ',
            ], {
                S: 'minecraft:stick',
                I: ingredient
            })
        }

        if (itemName.includes('_sickle')) {
            event.recipes.create.mechanical_crafting(itemName, [
                ' II',
                'IS ',
                ' S ',
            ], {
                S: 'minecraft:stick',
                I: ingredient
            })
        }

        if (itemName.includes('_katana')) {
            event.recipes.create.mechanical_crafting(itemName, [
                '  I',
                ' I ',
                'S  ',
            ], {
                S: 'minecraft:stick',
                I: ingredient
            })
        }
    })
}

ItemRemove.forEach((ItemRemove) => {
    if (ItemRemove.includes('warden')) {
        createItems(ItemRemove, 'minecraft:echo_shard')
    }
    if (ItemRemove.includes('electrum')) {
        createItems(ItemRemove, 'createaddition:electrum_ingot')
    }
})