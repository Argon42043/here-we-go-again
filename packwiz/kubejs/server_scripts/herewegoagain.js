
//Custom Recipies
ServerEvents.recipes(event => {


    //Polishing Recipies
    event.custom({
        type: 'create:sandpaper_polishing',
        ingredients: [
            { item: 'herewegoagain:superheated_steel' }
        ],
        results: [
            { item: 'herewegoagain:steel_crystal_seed', count: 10 }
        ]
    });

    //Haunting Recipies
    event.custom({
        type: 'create:haunting',
        ingredients: [
            { item: 'herewegoagain:ingot_steel' }
        ],
        results: [
            { item: 'herewegoagain:superheated_steel', count: 1 }
        ]
    });

    //Compacting Recipies
    event.custom({
        type: 'create:compacting',
        ingredients: [
            { item: 'herewegoagain:steel_flakes', count: 5 }
        ],
        results: [
            { item: 'herewegoagain:ingot_steel', count: 1 }
        ]
    });

    event.custom({
        type: 'create:pressing',
        ingredients: [
            { item: 'herewegoagain:ingot_steel' }
        ],
        results: [
            { item: 'herewegoagain:steel_plate', count: 1 }
        ]
    });

    //Crushing Recipies
    event.custom({
        type: 'create:crushing',
        ingredients: [
            { item: 'betterend:thallasium_ingot' }
        ],
        results: [
            { item: 'herewegoagain:crushed_thallasium', count: 1 }
        ]
    });

    event.custom({
        type: 'create:crushing',
        ingredients: [
            { item: 'betterend:raw_amber' }
        ],
        results: [
            { item: 'herewegoagain:crushed_amber', count: 1 }
        ]
    });

    event.custom({
        type: 'create:crushing',
        ingredients: [
            { item: 'herewegoagain:dirty_endbronze' }
        ],
        results: [
            { item: 'herewegoagain:crushed_dirty_endbronze', count: 1 }
        ]
    });

    event.custom({
        type: 'create:crushing',
        ingredients: [
            { item: 'herewegoagain:steel_slag' }
        ],
        results: [
            { item: 'herewegoagain:steel_flakes', count: 1, chance: 0.5 }
        ]
    });

    event.custom({
        type: 'create:crushing',
        ingredients: [
            { item: 'betternether:cincinnasite_ingot' }
        ],
        results: [
            { item: 'herewegoagain:crushed_cincinnasite', count: 2 }
        ]
    });

    event.custom({
        type: 'create:crushing',
        ingredients: [
            { item: 'minecraft:coal' }
        ],
        results: [
            { item: 'herewegoagain:crushed_coal', count: 1 }
        ]
    });

    //Mixing Recipies
    event.custom({
        type: 'create:mixing',
        ingredients: [
            { item: 'herewegoagain:crushed_thallasium' },
            { item: 'herewegoagain:crushed_amber' },
            { item: 'herewegoagain:catalyst' }
        ],
        results: [
            { item: 'herewegoagain:end_catalyst', count: 2 }
        ]
    });

    event.custom({
        type: 'create:mixing',
        ingredients: [
            { item: 'herewegoagain:end_catalyst' },
            { item: 'create:crushed_raw_copper' }
        ],
        results: [
            { item: 'herewegoagain:dirty_endbronze', count: 1 }
        ],
        heatRequirement: 'superheated'
    });

    event.custom({
        type: 'create:mixing',
        ingredients: [
            { item: 'herewegoagain:crushed_dirty_endbronze' },
            { item: 'betterend:filalux' }
        ],
        results: [
            { item: 'herewegoagain:crushed_clean_endbronze', count: 1 }
        ]
    });

    event.custom({
        type: 'create:mixing',
        ingredients: [
            { item: 'herewegoagain:crushed_cincinnasite' },
            { item: 'herewegoagain:catalyst' }
        ],
        results: [
            { item: 'herewegoagain:nether_catalyst', count: 1 }
        ]
    });

    event.custom({
        type: 'create:mixing',
        ingredients: [
            { item: 'herewegoagain:nether_catalyst' },
            { item: 'herewegoagain:steel_crystal_seed' }
        ],
        results: [
            { item: 'herewegoagain:activated_steel_seed', count: 1 }
        ]
    });

    event.custom({
        type: 'create:mixing',
        ingredients: [
            { item: 'herewegoagain:activated_steel_seed' },
            { item: 'herewegoagain:steel_mixture' }
        ],
        results: [
            { item: 'herewegoagain:seeded_steel_mixture', count: 1 }
        ]
    });



    //Blasting recipies
    event.blasting('herewegoagain:ingot_bronze', 'herewegoagain:crushed_clean_endbronze')
    event.blasting('herewegoagain:steel_slag', 'herewegoagain:steel_mixture')
    event.blasting('herewegoagain:ingot_steel', 'herewegoagain:seeded_steel_mixture')

    //Shapeless Recipies
    event.shapeless(
        Item.of('herewegoagain:catalyst', 2),
        [
          'betterend:charnia_red',
          'betterend:charnia_purple',
          'betterend:charnia_orange',
          'betterend:charnia_light_blue',
          'betterend:charnia_cyan',
          'betterend:charnia_green'
        ]
      )

    event.shapeless(
        Item.of('herewegoagain:steel_mixture', 1),
        [
          'herewegoagain:crushed_coal',
          'create:crushed_raw_iron'
        ]
      )
});
