ServerEvents.recipes(event => {
    event.remove({ output: 'multipiston:multipistonblock'})
})

ServerEvents.recipes(event => {
    event.recipes.create.mechanical_crafting('multipiston:multipistonblock', [
        'SLS',
        'GRV',
        'pPp',
    ], {
        R: 'minecraft:redstone_block',
        p: 'create:sticky_mechanical_piston',
        P: 'minecraft:sticky_piston',
        V: 'create:vertical_gearbox',
        G: 'create:gearbox',
        S: 'minecraft:stone',
        L: 'create:linked_controller'
    })
})