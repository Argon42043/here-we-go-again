ServerEvents.tags('item', (event) => {
    event.removeAll('createbigcannons:ingot_steel')
    Ingredient.all.itemIds.forEach(itemId => {
        if (itemId.toString().includes('ingot_steel')) {
            event.add('createbigcannons:ingot_steel', itemId);
        }
    });

    event.removeAll('createbigcannons:ingot_bronze')
    Ingredient.all.itemIds.forEach(itemId => {
        if (itemId.toString().includes('ingot_bronze')) {
            event.add('createbigcannons:ingot_bronze', itemId);
        }
    });
});

ServerEvents.recipes(event => {
    
    //Remove the recipie for making ingots from molten
    event.remove({ id: 'createbigcannons:compacting/forge_steel_ingot' })
    event.remove({ id: 'createbigcannons:compacting/forge_bronze_ingot' })
    event.remove({ id: 'createbigcannons:cutting/spring_wire_steel' })
    event.remove({ id: 'createbigcannons:cutting/spring_wire_iron' })

    //Coil Recipie
    event.custom({
        type: 'createaddition:rolling',
        ingredients: [
            { item: 'herewegoagain:steel_plate' }
        ],
        results: [
            { item: 'createbigcannons:spring_wire', count: 1 }
        ]
    });
});
