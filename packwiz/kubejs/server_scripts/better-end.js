// Listen to item tag event
ServerEvents.tags('item', event => {

  //Remove Thalassium Ingot from Collection to not make it go with iron
  event.removeAllTagsFrom("betterend:thallasium_ingot");
})

ServerEvents.recipes(event => {
  event.remove({ output: 'betterend:elytra_armored' })
})
