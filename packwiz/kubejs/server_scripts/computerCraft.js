ServerEvents.recipes(event => {
    event.remove({ output: 'computercraft:monitor_normal'})
    event.remove({ output: 'computercraft:monitor_advanced'})
    event.remove({ output: 'computercraft:computer_normal'})
    event.remove({ id: 'computercraft:computer_advanced'})
    event.remove({ id: 'computercraft:computer_advanced_upgrade'})
    event.remove({ id: 'computercraft:computer_command'})
    event.remove({ id: 'computercraft:pocket_computer_normal'})
    event.remove({ id: 'computercraft:pocket_computer_advanced_upgrade'})
    event.remove({ id: 'computercraft:pocket_computer_advanced'})
    event.remove({ output: 'computercraft:disk_drive'})
    event.remove({ output: 'computercraft:speaker'})
    event.remove({ output: 'computercraft:printer'})
    event.remove({ output: 'computercraft:wireless_modem_normal'})
    event.remove({ output: 'computercraft:wireless_modem_advanced'})
    event.remove({ id: 'computercraft:turtle_normal'})
    event.remove({ id: 'computercraft:turtle_advanced_upgrade'})
    event.remove({ id: 'computercraft:turtle_advanced'})
    event.remove({ output: 'computercraft:cable'})
    event.remove({ output: 'computercraft:wired_modem_full'})
    event.remove({ output: 'computercraft:wired_modem'})
})

ServerEvents.recipes(event => {
    event.recipes.create.mechanical_crafting('computercraft:monitor_normal', [
        'IDDDI',
        'ISSSI',
        'IPFPI',
        'IWLWI',
        'IIIII'
    ], {
        W: 'createaddition:copper_wire',
        L: 'minecraft:redstone_lamp',
        F: 'minecraft:spyglass',
        S: 'quark:framed_glass_pane',
        P: 'minecraft:prismarine_crystals',
        D: 'supplementaries:crystal_display',
        I: 'create:iron_sheet'
    })

    event.recipes.create.mechanical_crafting('computercraft:monitor_advanced', [
        'SRGBS',
        'SOEOS',
        'SPMCS',
        'SWTWS',
        'SSSSS'
    ], {
        E: 'minecraft:end_crystal',
        S: 'createaddition:electrum_sheet',
        R: 'quark:red_shard',
        G: 'quark:green_shard',
        B: 'quark:blue_shard',
        W: 'createaddition:gold_wire',
        T: 'createaddition:tesla_coil',
        C: 'createaddition:redstone_relay',
        P: 'create:propeller',
        M: 'computercraft:monitor_normal',
        O: 'create:electron_tube'
    })

    event.recipes.create.mechanical_crafting('computercraft:computer_normal', [
        'IIIIIII',
        'ILGDGSI',
        'ICRORCI',
        'IDApADI',
        'ICMOMCI',
        'IPGDGPI',
        'IIIIIII'
    ], {
        I: 'create:iron_sheet',
        P: 'create:propeller',
        G: 'createaddition:gold_spool',
        C: 'createaddition:copper_spool',
        O: 'create:electron_tube',
        R: 'minecraft:comparator',
        D: 'minecraft:redstone',
        L: 'createaddition:large_connector',
        S: 'createaddition:connector',
        A: 'createaddition:capacitor',
        p: 'create:precision_mechanism',
        M: 'advancedperipherals:memory_card'
    })

    event.recipes.create.mechanical_crafting('computercraft:computer_advanced', [
        'EEEEEEE',
        'ESDODSE',
        'ErCpCrE',
        'EOPAPOE',
        'ErCPCrE',
        'ESDODSE',
        'EEEEEEE'
    ], {
        E: 'createaddition:electrum_sheet',
        p: 'create:precision_mechanism',
        P: 'create:propeller',
        r: 'createaddition:redstone_relay',
        C: 'computercraft:computer_normal',
        S: 'createaddition:electrum_spool',
        D: 'minecraft:redstone',
        O: 'create:electron_tube',
        A: 'minecraft:comparator'
    })

    event.recipes.create.mechanical_crafting('computercraft:pocket_computer_normal', [
        'IIIII',
        'BWRUB',
        'IaMAB',
        'IaCpI',
        'IaawI',
        'IISII'
    ], {
        I: 'createaddition:iron_rod',
        B: 'infinitybuttons:iron_button',
        W: 'createaddition:copper_wire',
        R: 'createaddition:redstone_relay',
        p: 'create:precision_mechanism',
        A: 'createaddition:capacitor',
        a: 'createaddition:modular_accumulator',
        w: 'createaddition:gold_wire',
        U: 'quark:abacus',
        C: 'computercraft:computer_normal',
        M: 'computercraft:monitor_normal',
        S: 'createaddition:connector'
    })

    event.recipes.create.mechanical_crafting('computercraft:pocket_computer_advanced', [
        'EEEEE',
        'EWpME',
        'EpCpE',
        'EAPOE',
        'EaWaE',
        'EELEE'
    ], {
        P: 'computercraft:pocket_computer_normal',
        M: 'computercraft:monitor_advanced',
        C: 'computercraft:computer_advanced',
        L: 'createaddition:large_connector',
        E: 'createaddition:electrum_rod',
        p: 'create:precision_mechanism',
        O: 'create:electron_tube',
        A: 'createaddition:capacitor',
        a: 'createaddition:modular_accumulator',
        W: 'createaddition:gold_wire'
    })

    event.recipes.create.mechanical_crafting('computercraft:disk_drive', [
        'IIIII',
        'IEROI',
        'ITLMI',
        'ISECI',
        'IIIII'
    ], {
        I: 'create:iron_sheet',
        E: 'createaddition:electric_motor',
        R: 'createaddition:iron_rod',
        L: 'createaddition:large_connector',
        S: 'createaddition:copper_spool',
        C: 'createaddition:capacitor',
        O: 'minecraft:observer',
        M: 'advancedperipherals:memory_card',
        T: 'create:electron_tube',
    })

    event.recipes.create.mechanical_crafting('computercraft:speaker', [
        'IGGGI',
        'ILLLI',
        'IWRWI',
        'ITSCI',
        'IIpII'
    ], {
        I: 'create:iron_sheet',
        T: 'create:electron_tube',
        p: 'create:precision_mechanism',
        G: 'graveyard:dark_iron_bars',
        L: 'sewingkit:leather_sheet',
        W: 'createaddition:copper_wire',
        R: 'createaddition:iron_rod',
        S: 'createaddition:copper_spool',
        C: 'createaddition:capacitor',
    })

    event.recipes.create.mechanical_crafting('computercraft:printer', [
        'IIIII',
        'ICcLI',
        'IpBSI',
        'IEMEI',
        'IIIII'
    ], {
        I: 'create:iron_sheet',
        C: 'createaddition:capacitor',
        M: 'advancedperipherals:memory_card',
        E: 'createaddition:electric_motor',
        S: 'create:shaft',
        B: 'create:belt_connector',
        p: 'create:precision_mechanism',
        L: 'createaddition:large_connector',
        c: 'create:linked_controller'
    })

    event.recipes.create.mechanical_crafting('computercraft:wireless_modem_normal', [
        'RRRS',
        'RPTR',
        'RpWR',
        'CRRR',
    ], {
        R: 'createaddition:iron_rod',
        p: 'create:precision_mechanism',
        P: 'create:propeller',
        T: 'createaddition:tesla_coil',
        S: 'minecraft:sculk_sensor',
        W: 'createaddition:gold_wire',
        C: 'createaddition:connector'
    })

    event.recipes.create.mechanical_crafting('computercraft:wireless_modem_advanced', [
        'RRRS',
        'RTLR',
        'RMFR',
        'CRRR'
    ], {
        R: 'createaddition:electrum_rod',
        S: 'minecraft:sculk_shrieker',
        L: 'create:redstone_link',
        T: 'create:electron_tube',
        M: 'computercraft:wireless_modem_normal',
        F: 'minecraft:spyglass',
        C: 'createaddition:large_connector'
    })

    event.recipes.create.mechanical_crafting('computercraft:turtle_normal', [
        'IIIII',
        'ITACI',
        'IBNOI',
        'IpPEI',
        'IIIII'
    ], {
        I: 'create:iron_sheet',
        T: 'create:electron_tube',
        A: 'createaddition:modular_accumulator',
        C: 'createaddition:capacitor',
        p: 'create:precision_mechanism',
        P: 'create:propeller',
        E: 'create:steam_engine',
        B: 'minecraft:barrel',
        N: 'computercraft:computer_normal',
        O: 'createaddition:alternator'
    })

    event.recipes.create.mechanical_crafting('computercraft:turtle_advanced', [
        'IIIII',
        'ITAAI',
        'IBNOI',
        'IpPEI',
        'IIIII'
    ], {
        I: 'createaddition:electrum_sheet',
        T: 'create:electron_tube',
        A: 'createaddition:modular_accumulator',
        p: 'create:precision_mechanism',
        P: 'create:propeller',
        E: 'create:steam_engine',
        B: 'create:item_vault',
        N: 'computercraft:computer_advanced',
        O: 'createaddition:alternator'
    })

    event.shapeless(
        Item.of('computercraft:cable', 1),
        [
            'sewingkit:wool_trim',
            'createaddition:copper_wire'
        ]
    )

    event.shapeless(
        Item.of('computercraft:wired_modem_full', 1),
        [
            'create:iron_sheet',
            'create:electron_tube',
            'createaddition:capacitor',
            'createaddition:copper_spool',
            'createaddition:redstone_relay',
        ])

    event.shapeless(
        Item.of('computercraft:wired_modem_full', 1),
        [
            'computercraft:wired_modem'
        ]
    )

    event.shapeless(
        Item.of('computercraft:wired_modem', 1),
        [
            'computercraft:wired_modem_full'
        ]
    )
})
