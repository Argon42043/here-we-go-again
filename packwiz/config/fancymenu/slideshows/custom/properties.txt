type = slideshow

slideshow-meta {
   name = slideshow
   width = 2560
   height = 1440
   x = 0
   y = 0
   duration = 6.0
   fadespeed = 3.0
}