scoreboard players add @a money_payout 0

execute as @a[scores={money_payout=0,money_delay=1..}] run scoreboard players remove @s money_delay 1
execute as @a[scores={money_payout=0}] unless score @s money_delay = @s money_delay run scoreboard players set @s money_delay 300
execute as @a[scores={money_payout=0,money_delay=0}] run function money:payout

# reset counter if player left the game
scoreboard players reset * money_delay_tmp
execute as @a run scoreboard players operation @s money_delay_tmp = @s money_delay
scoreboard players reset * money_delay
execute as @a run scoreboard players operation @s money_delay = @s money_delay_tmp
execute as @a[scores={money_delay=0}] run scoreboard players reset @s money_delay
# loop
schedule function money:loop 1s
